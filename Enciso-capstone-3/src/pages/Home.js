import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "MA Bookstore",
		content: "Navigating the Code, Building Your Skills – Your Source for Web Development Books.",
		destination: "/products",
		label: "View Collections"
	}


	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
}


