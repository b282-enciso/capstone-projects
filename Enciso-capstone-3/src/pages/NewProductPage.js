import React, { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate } from 'react-router-dom'; 
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function CreateProduct() {
  
  const navigate = useNavigate();
  const {user, setUser} = useContext(UserContext);

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");
  const [isActive, setIsActive] = useState(false);

  const productData = {
    'name':name,
    'description':description,
    'price': parseFloat(price), 
  };
   
  useEffect(() => {
    if (name !== "" && description !== "" && price !== "")  {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [name, description, price]);

  function addNewProduct(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
         'Authorization': `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(productData),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          setName("");
          setDescription("");
          setPrice("");

          Swal.fire({
            title: "New Product Created",
            icon: "success",
            text: "Product Added." 
          });

          navigate("/admin_dashboard");

        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again."
          });
        }
      })
      .catch((error) => {
      console.error("Error creating new product:", error);

      // Handle the error, show an error message, or redirect to an error page
      Swal.fire({
        title: "Error",
        icon: "error",
        text: "Something went wrong. Please try again later.",
      });
    });
  }

  return (

    <div className="container mt-5">
      <h1 className="text-center">Add New Product </h1>
      <Form onSubmit={(e) => addNewProduct (e)}>

        <Form.Group controlId="productName">
          <Form.Label>Product Name</Form.Label>
          <Form.Control
            type="text"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="productDescription">
          <Form.Label>Product Description</Form.Label>
          <Form.Control
            as="textarea"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            required
          />
        </Form.Group>
        <Form.Group controlId="productPrice">
          <Form.Label>Product Price</Form.Label>
          <Form.Control
            type="number"
            step="0.01"
            value={price}
            onChange={(e) => setPrice(e.target.value)}
            required
          />
        </Form.Group>
        <Button variant="primary" type="submit">
          Create Product
        </Button>

      </Form>
    </div>
  );
}
