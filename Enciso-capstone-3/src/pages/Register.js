import React, { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Form, Button, Row, Col } from 'react-bootstrap';

export default function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [firstName, setFirstName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    if (
      firstName !== "" &&
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      (password1 === password2)
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, email, password1, password2]);

  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
      method: "POST",
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email
      })
    })
      .then(res => res.json())
      .then(data => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Duplicate Email Found",
            icon: "error",
            text: "The email address you provided is already registered!"
          });
        } else {
          fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
            method: "POST",
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              firstName: firstName,
              email: email,
              password: password1
            })
          })
            .then(res => res.json())
            .then(data => {
              console.log(data);

              if (data === true) {
                setFirstName("");
                setEmail("");
                setPassword1("");
                setPassword2("");

                Swal.fire({
                  title: "Registration Successful",
                  icon: "success",
                  text: "Welcome to MA's Bookstore!"
                });

                navigate("/login");
              } else {
                Swal.fire({
                  title: "Something went wrong",
                  icon: "error",
                  text: "Please, try again."
                });
              }
            });
        }
      });
  }

  return (
    (user.id !== null) ? <Navigate to="/products" /> :
      <Form onSubmit={(e) => registerUser(e)} style={{ marginLeft: '25%', marginRight: '25%' }}>
        <h2 className="text-center my-3">Register Account</h2>
        <Form.Group className="mb-3" controlId="userFirstName">
          <Form.Label>First Name</Form.Label>
          <Form.Control
            type="text"
            value={firstName}
            onChange={(e) => { setFirstName(e.target.value) }}
            placeholder="Enter your first name"
            required
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            value={email}
            onChange={(e) => { setEmail(e.target.value) }}
            placeholder="Enter your email"
            required
          />
          <Form.Text className="text-muted">
            Don't worry! We never share your details with anyone else.
          </Form.Text>
        </Form.Group>
        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password1}
            onChange={(e) => { setPassword1(e.target.value) }}
            placeholder="Enter Your Password"
            required
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control
            type="password"
            value={password2}
            onChange={(e) => { setPassword2(e.target.value) }}
            placeholder="Verify Your Password"
            required
          />
        </Form.Group>
        <Row>
          <Col xs={{ span: 6 }}>
            {isActive ?
              <Button variant="success" type="submit" id="submitBtn">
                Register Now
              </Button>
              :
              <Button variant="success" type="submit" id="submitBtn" disabled>
                Register Now
              </Button>
            }
          </Col>
        </Row>
        <p className="text-center mt-3">Already have an account? <a href="/login">Login here</a></p>
      </Form>
  );
}
