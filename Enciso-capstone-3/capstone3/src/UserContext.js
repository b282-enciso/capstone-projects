// UserContext.js
import React from 'react';

const UserContext = React.createContext({
  isAuthenticated: false,
  setIsAuthenticated: () => {},
  // Add any other user-specific data or functions if needed
});

export const UserProvider = UserContext.Provider;
export const UserConsumer = UserContext.Consumer;

export default UserContext;
