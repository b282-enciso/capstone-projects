import { useState, useEffect } from 'react';

import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';

import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import AdminDashboard from './pages/AdminPanel';
import NewProductPage from './pages/NewProductPage';
import UpdateProductPage from './pages/UpdateProductPage';

import './App.css';

import { Container } from 'react-bootstrap';

import { UserProvider } from './UserContext';

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {
  const [user, setUser] = useState({
    id: null,
    isAdmin: null,
  });

  const unsetUser = () => {
    localStorage.removeItem('token');
    setUser({
      id: null,
      isAdmin: null
    });
  };

  useEffect(() => {
      const token = localStorage.getItem('token');

      if (token) {
          fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/users/details`, {
              headers: {
                  Authorization: `Bearer ${token}`
              }
          })
          .then(res => {
              if (res.status === 401) {
                  unsetUser();  // Unset user if unauthorized (token expired or invalid)
                  console.error('Unauthorized request. Maybe the token is expired or invalid.');
                  return;
              }
              return res.json();
          })
          .then(data => {
              if (data && data._id) {
                  setUser({
                      id: data._id,
                      isAdmin: data.isAdmin
                  });
              } else {
                  unsetUser();  // Ensure to unset user if data isn't as expected
              }
          })
          .catch(error => {
              console.error('Error:', error);
          });
      }
  }, []);

  return (
    <>
      <UserProvider value={{ user, setUser, unsetUser }}>
        <Router>
          <AppNavbar />
          <Container>
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/register" element={<Register />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />} />
              <Route path="/admin_dashboard" element={<AdminDashboard />} />
              <Route path="/products/new" element={<NewProductPage />} />
              <Route path="/products/:productId/update" element={<UpdateProductPage />} />
              <Route path="/*" element={<Error />} />
            </Routes>
          </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
