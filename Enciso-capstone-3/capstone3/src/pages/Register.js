import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { Form, Button } from 'react-bootstrap';

function Register() {
  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
    setIsActive(email !== "" && password1 === password2 && password1 !== "");
  }, [email, password1, password2]);

  function registerUser(e) {
    e.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/users/register`, {
      method: "POST",
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: email,
        password: password1
      })
    })
      .then(res => res.json())
      .then(data => {
        if (data === true) {
          setEmail("");
          setPassword1("");
          setPassword2("");

          Swal.fire({
            title: "Registration Successful",
            icon: "success",
            text: "Welcome to MA Bookstore!"
          });

          navigate("/login");
        } else if (data.message && data.message.includes("Duplicate email")) {
          Swal.fire({
            title: "Duplicate Email Found",
            icon: "error",
            text: "Kindly provide another email to complete registration."
          });
        } else {
          Swal.fire({
            title: "Something went wrong",
            icon: "error",
            text: "Please, try again."
          });
        }
      });
  }

  return user.id !== null ? (
    <Navigate to="/products" />
  ) : (
    <div style={{ maxWidth: '400px', margin: '0 auto', padding: '20px' }}>
      <Form onSubmit={(e) => registerUser(e)}>
        <h1 className="text-center my-3">Registration</h1>
        <Form.Group className="mb-3" controlId="userEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control
            type="email"
            value={email}
            onChange={(e) => { setEmail(e.target.value) }}
            placeholder="Enter your email"
            autoComplete="email"
          />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="password1">
          <Form.Label>Password</Form.Label>
          <Form.Control
            type="password"
            value={password1}
            onChange={(e) => { setPassword1(e.target.value) }}
            placeholder="Enter Your Password"
            autoComplete="new-password"
          />
        </Form.Group>

        <Form.Group className="mb-3" controlId="password2">
          <Form.Label>Verify Password</Form.Label>
          <Form.Control
            type="password"
            value={password2}
            onChange={(e) => { setPassword2(e.target.value) }}
            placeholder="Verify Your Password"
            autoComplete="new-password"
          />
        </Form.Group>

        <div style={{ textAlign: 'center', marginTop: '20px' }}>
          <Button variant="primary" type="submit" id="submitBtn" disabled={!isActive}>
            Register
          </Button>
        </div>
      </Form>
      <p style={{ textAlign: 'center', marginTop: '20px' }}>
        Already have an account? <a href="/login" style={{ color: '#007bff' }}>Log in here</a>
      </p>
    </div>
  );
}

export default Register;
