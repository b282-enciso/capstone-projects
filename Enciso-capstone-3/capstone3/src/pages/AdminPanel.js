import React from 'react';
import AdminControlPanel from '../components/AdminControlPanel';

function AdminDashboard() {
    return (
        <div>
            <h1>Admin Dashboard</h1>
            <AdminControlPanel />
        </div>
    );
}

export default AdminDashboard;
