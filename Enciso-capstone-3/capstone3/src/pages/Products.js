import ProductCard from '../components/ProductCard';

import {useState, useEffect} from 'react';



export default function Products() {

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/active`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => <ProductCard key={product._id} product={product} />));

		})
	}, []);



	return (
		<>
		{products}
		</>
	)
};





