import {useContext, useEffect} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

export default function Logout() {
    const {unsetUser} = useContext(UserContext);

    useEffect(() => {
        unsetUser();
       
    }, []);
    
    return <Navigate to="/login" />;
}
