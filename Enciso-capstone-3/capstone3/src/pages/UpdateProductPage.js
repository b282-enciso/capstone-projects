import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';

function UpdateProductPage() {
    const [product, setProduct] = useState(null);
    const [formValues, setFormValues] = useState({name: '', description: '', price: ''});
    
    const { productId } = useParams();
    const token = localStorage.getItem('token');

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/${productId}`, {
            headers: { 
                'Authorization': `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(data => {
            setProduct(data);
            setFormValues(data);   // Setting the fetched data to formValues
        })
        .catch(error => {
            console.error("Failed to fetch product:", error);
            // Consider updating the UI with this error message.
        });
    }, [productId, token]);

    const handleChange = (field) => (e) => {  // Step 2
        setFormValues({
            ...formValues,
            [field]: e.target.value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/${productId}`, {
            method: 'PUT',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(formValues)
        })
        .then(response => response.json())
        .then(data => {
            if(data.success) {
                alert("Product updated successfully!");
            } else {
                throw new Error(data.message);
            }
        })
        .catch(error => alert("Failed to update product: " + error.message));
    };

    if (!product) return <div>Loading...</div>;

   return (
         <div className="update-product">
             <h2>Update Product: {product.name}</h2>
             <form onSubmit={handleSubmit}>
                 <div className="input-group">
                     <label htmlFor="name">Name</label>
                     <input type="text" id="name" value={formValues.name} onChange={handleChange('name')} />
                 </div>
                 <div className="input-group">
                     <label htmlFor="price">Price</label>
                     <input type="text" id="price" value={formValues.price} onChange={handleChange('price')} />
                 </div>
                 <div className="input-group">
                     <label htmlFor="description">Description</label>
                     <textarea id="description" value={formValues.description} onChange={handleChange('description')}></textarea>
                 </div>
                 <button type="submit">Update</button>
             </form>
         </div>
       );
   }

   export default UpdateProductPage;
