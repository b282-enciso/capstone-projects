import React, { useState } from 'react';

function NewProductPage() {
    const [product, setProduct] = useState({
        name: "",
        description: "",
        price: 0,
    });
    const token = localStorage.getItem('token');

    const handleSubmit = (e) => {
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products`, {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(product)
        })
        .then(response => response.json())
        .then(data => {
            if(data.success) {
                alert("Product added successfully!");
            } else {
                throw new Error(data.message);
            }
        })
        .catch(error => alert("Failed to add product: " + error.message));
    };

    return (
        <div>
            <h2>Add New Product</h2>
            <form onSubmit={handleSubmit}>
                <input 
                    type="text" 
                    placeholder="Product Name" 
                    value={product.name} 
                    onChange={e => setProduct({ ...product, name: e.target.value })} 
                />
                <textarea 
                    placeholder="Description" 
                    value={product.description} 
                    onChange={e => setProduct({ ...product, description: e.target.value })} 
                />
                <input 
                    type="number" 
                    placeholder="Price" 
                    value={product.price} 
                    onChange={e => setProduct({ ...product, price: parseFloat(e.target.value) })} 
                />
                <button type="submit">Add Product</button>
            </form>
        </div>
    );
}

export default NewProductPage;
