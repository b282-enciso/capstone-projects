import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

  const data = {
    title: "MA Bookstore",
    content: "From Noob to Expert Web Developer!",
    destination: "/products",
    label: "View Our Collection"
  }


  return (
    <>
    <Banner data={data} />
      <Highlights />
      
    </>
  )
}