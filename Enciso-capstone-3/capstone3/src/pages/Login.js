import React, { useState, useContext } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import UserContext from '../UserContext';

const API_URL = 'http://localhost:4001';

const Login = () => {
    const { setUser } = useContext(UserContext);
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState('');
    const navigate = useNavigate();

    const login = async (email, password) => {
        try {
            const response = await axios.post(`${API_URL}/users/login`, { email, password });
            
            if (response.status === 200 && response.data) {
                const { access, isAdmin, _id } = response.data;
                localStorage.setItem('token', access);

                return {
                    success: true,
                    isAdmin,
                    data: { _id }
                };
            }
            return { success: false };
        } catch (err) {
            throw new Error(err.response ? err.response.data.message : 'Error connecting to server');
        }
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            const response = await login(email, password);
                
            if (response.success) {
                setUser({
                    id: response.data._id, 
                    isAdmin: response.isAdmin
                });

                navigate(response.isAdmin ? '/admin/dashboard' : '/products');
            } else {
                setError('Invalid credentials');
            }
        } catch (err) {
            setError(err.message || 'There was an issue logging in. Please try again later.');
        }
    };

    return (
        <div style={{ maxWidth: '400px', margin: '0 auto', padding: '20px' }}>
            <h2 style={{ textAlign: 'center', marginBottom: '20px' }}>Login</h2>
            {error && <p className="error" style={{ backgroundColor: 'red', color: 'white', padding: '10px', borderRadius: '5px' }}>{error}</p>}
            <form onSubmit={handleSubmit} style={{ display: 'flex', flexDirection: 'column', gap: '10px' }}>
                <div style={{ display: 'flex', flexDirection: 'column', gap: '5px' }}>
                    <label>Email</label>
                    <input 
                        type="email" 
                        value={email} 
                        onChange={(e) => setEmail(e.target.value)}
                        autoComplete="username"
                        style={{ padding: '10px', borderRadius: '5px', border: '1px solid #ccc' }}
                    />
                </div>
                <div style={{ display: 'flex', flexDirection: 'column', gap: '5px' }}>
                    <label>Password</label>
                    <input 
                        type="password" 
                        value={password} 
                        onChange={(e) => setPassword(e.target.value)}
                        autoComplete="current-password"
                        style={{ padding: '10px', borderRadius: '5px', border: '1px solid #ccc' }}
                    />
                </div>
                <div style={{ textAlign: 'center', marginTop: '20px' }}>
                    <button type="submit" style={{ padding: '10px 20px', borderRadius: '5px', backgroundColor: '#007bff', color: 'white', border: 'none', cursor: 'pointer' }}>
                        Login
                    </button>
                </div>
            </form>
            <p style={{ textAlign: 'center', marginTop: '20px' }}>
                No account yet? <a href="/register" style={{ color: '#007bff' }}>Register here</a>
            </p>
        </div>
    );
};

export default Login;
