import { useState, useContext, useEffect } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, Link, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {
    const { user } = useContext(UserContext);
    const navigate = useNavigate();
    const { productId } = useParams();

    const [productName, setProductName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);
    const [totalAmount, setTotalAmount] = useState(0);

    const handleQuantityChange = (event) => {
        const newQuantity = parseInt(event.target.value, 10);
        setQuantity(newQuantity);
        setTotalAmount(price * newQuantity);
    };

    const checkout = () => {
        if (user.isAdmin) {
            Swal.fire({
                title: 'Not Allowed',
                icon: 'warning',
                text: 'Admins are not allowed to checkout!',
            });
            return;
        }

        const requestBody = {
            productId: productId,
            productName: productName,
            quantity: quantity,
            totalAmount: totalAmount,
        };

        fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/users/checkout`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`,
            },
            body: JSON.stringify(requestBody),
        })
            .then((res) => res.json())
            .then((data) => {
                if (data) {
                    Swal.fire({
                        title: 'Checkout Successful!',
                        icon: 'success',
                        text: 'You have placed an order.',
                    });
                    navigate('/products');
                } else {
                    Swal.fire({
                        title: 'Something went wrong',
                        icon: 'error',
                        text: 'Please try again.',
                    });
                }
            })
            .catch((error) => {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Something went wrong. Please try again later.',
                });
            });
    };

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/${productId}`)
            .then((res) => res.json())
            .then((data) => {
                setProductName(data.name);
                setDescription(data.description);
                setPrice(data.price);
                setTotalAmount(data.price * quantity); 
            })
            .catch((error) => {
                console.error('Error fetching product details:', error);
            });
    }, [productId, quantity]);

   return (
           <Container>
               <Row className="my-5">
                   <Col lg={{ span: 8, offset: 2 }}>
                       <Card className="shadow-lg">
                           <Card.Body>
                               <Row>
                                   {/* Product details section */}
                                   <Col lg={8}>
                                       <Card.Title className="font-weight-bold">{productName}</Card.Title>
                                       <Card.Subtitle className="mb-2 text-muted">Description</Card.Subtitle>
                                       <Card.Text>{description}</Card.Text>
                                       <Card.Subtitle className="mb-2 text-muted">Price</Card.Subtitle>
                                       <Card.Text className="font-weight-bold">PhP {price.toFixed(2)}</Card.Text>
                                   </Col>
                                   {/* Checkout section */}
                                   <Col lg={4}>
                                       <Card.Subtitle className="mb-2 text-muted">Quantity</Card.Subtitle>
                                       <Form.Group controlId="quantityInput">
                                           <Form.Control
                                               type="number"
                                               value={quantity}
                                               onChange={handleQuantityChange}
                                           />
                                       </Form.Group>
                                       <Card.Subtitle className="mb-2 text-muted">Total Amount</Card.Subtitle>
                                       <Card.Text className="font-weight-bold">PhP {totalAmount.toFixed(2)}</Card.Text>
                                       {user.id !== null ? (
                                           <Button variant="primary" onClick={() => checkout(productId)}>
                                               Checkout
                                           </Button>
                                       ) : (
                                           <Button className="btn btn-danger" as={Link} to="/login">
                                               Log in to Checkout
                                           </Button>
                                       )}
                                   </Col>
                               </Row>
                           </Card.Body>
                       </Card>
                       <Row className="mt-3">
                           <Col>
                               <Button as={Link} to="/products" variant="outline-secondary">
                                   ← Back to Products
                               </Button>
                           </Col>
                       </Row>
                   </Col>
               </Row>
           </Container>
       );
   }
