import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { useContext } from 'react';  
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

function AppNavbar() {
  const { user } = useContext(UserContext);

  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand as={Link} to="/">
        <h3>MA Bookstore - </h3>
          {' '}From Noob to Expert Web Developer
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">  {/* 'ms-auto' pushes the navigation items to the right */}
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>

            {/* Conditionally render links based on user's authentication and role */}
            {user.id ? (
              <>
                <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
                {user.isAdmin && <Nav.Link as={NavLink} to="/admin_dashboard">Admin Dashboard</Nav.Link>}
              </>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppNavbar;
