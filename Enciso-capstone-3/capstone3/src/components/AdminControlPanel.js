import React, { useContext, useState, useEffect } from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


function AdminControlPanel() {
    const { user } = useContext(UserContext);
    const [products, setProducts] = useState([]);

    useEffect(() => {
        fetchProducts();
    }, [user]);

    const fetchProducts = () => {
        if (user && user.isAdmin) {
            const token = localStorage.getItem('token');
             console.log("Token from localStorage:", token);

            fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/all`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                if (data && Array.isArray(data)) {
                    setProducts(data);
                }
            })
            .catch(error => {
                console.error('Failed to fetch products:', error);
            });
        }
    };

   const archiveProduct = (productId) => {
       const token = localStorage.getItem('token');

       fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/${productId}/archive`, {
           method: 'PATCH',
           headers: {
               Authorization: `Bearer ${token}`
           }
       })
       .then(response => {
           if (!response.ok) {
               return response.json().then(err => { throw err });
           }
           return response.json();
       })
       .then(data => {
           Swal.fire('Success', 'Product archived successfully!', 'success');
           fetchProducts();
       })
       .catch(error => {
           Swal.fire('Error', `Error archiving product: ${error.message || 'Unknown error'}`, 'error');
       });
   };


    const activateProduct = (productId) => {
        const token = localStorage.getItem('token');

        fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/${productId}/activate`, {
            method: 'PATCH',
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => {
            return response.json().then(data => {
                if (!response.ok) {
                    throw new Error(data.message || 'Error activating product');
                }
                return data;
            });
        })
        .then(data => {
            if (data.success) {
                Swal.fire('Success', 'Product activated successfully!', 'success');
                fetchProducts();
            } else {
                throw new Error(data.message);
            }
        })
        .catch(error => {
            Swal.fire('Error', `Error activating product: ${error.message || 'Unknown error'}`, 'error');
        });
    };


   const updateProduct = async (product) => {
       let newName, newDescription, newPrice;

       try {
           const nameResult = await Swal.fire({
               title: 'Product Name',
               text: 'Enter the name of the product',
               input: 'text',
               inputValue: product.name,
               showCancelButton: true
           });
           if (!nameResult.value) return;
           newName = nameResult.value;

           const descriptionResult = await Swal.fire({
               title: 'Description',
               text: 'Enter the product description',
               input: 'text',
               inputValue: product.description,
               showCancelButton: true
           });
           if (!descriptionResult.value) return;
           newDescription = descriptionResult.value;

           const priceResult = await Swal.fire({
               title: 'Price',
               text: 'Enter the product price',
               input: 'text',
               inputValue: product.price.toString(),
               showCancelButton: true
           });
           if (!priceResult.value || isNaN(parseFloat(priceResult.value))) {
               Swal.fire('Error', 'Price should be a valid number!', 'error');
               return;
           }
           newPrice = parseFloat(priceResult.value);
       } catch (error) {
           Swal.fire('Error', `Failed to update product: ${error.message || 'Unknown error'}`, 'error');
           return;
       }

       const updatedProduct = {
           ...product,
           name: newName,
           description: newDescription,
           price: newPrice
       };

       const token = localStorage.getItem('token');

       fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/${product._id}/update`, {
           method: 'PUT',
           headers: {
               'Content-Type': 'application/json',
               'Authorization': `Bearer ${token}`
           },
           body: JSON.stringify(updatedProduct)
       })
       .then(response => {
           if (!response.ok) {
               return response.json().then(err => { throw err });
           }
           return response.json();
       })
       .then(data => {
           Swal.fire('Success', 'Product updated successfully!', 'success');
           fetchProducts();
       })
       .catch(error => {
           Swal.fire('Error', `Error updating product: ${error.message || 'Unknown error'}`, 'error');
       });
   };
   const createProduct = async () => {
       try {
           const { value: productName } = await Swal.fire({
               title: 'Product Name',
               input: 'text',
               inputLabel: 'Enter the name of the product',
               showCancelButton: true
           });

           if (!productName) return;

           const { value: productDescription } = await Swal.fire({
               title: 'Description',
               input: 'text',
               inputLabel: 'Enter the product description',
               showCancelButton: true
           });

           if (!productDescription) return;

           const { value: productPrice } = await Swal.fire({
               title: 'Price',
               input: 'text',
               inputLabel: 'Enter the product price',
               showCancelButton: true,
               inputValidator: (value) => {
                   if (isNaN(parseFloat(value))) {
                       return 'Price should be a valid number!';
                   }
               }
           });

           if (!productPrice) return;

           const newProduct = {
               name: productName,
               description: productDescription,
               price: parseFloat(productPrice)
           };

           const token = localStorage.getItem('token');

           fetch(`${process.env.REACT_APP_API_URL || 'http://localhost:4001'}/products/create`, {
               method: 'POST',
               headers: {
                   'Content-Type': 'application/json',
                   'Authorization': `Bearer ${token}`
               },
               body: JSON.stringify(newProduct)
           })
           .then(response => {
               if (!response.ok) {
                   return response.json().then(err => { throw err });
               }
               return response.json();
           })
           .then(data => {
               Swal.fire('Success', 'Product created successfully!', 'success');
               fetchProducts();
           })
           .catch(error => {
               Swal.fire('Error', `Error creating product: ${error.message || 'Unknown error'}`, 'error');
           });

       } catch (error) {
           Swal.fire('Cancelled', 'Product creation was cancelled or an error occurred.', 'error');
       }
   };

  return (
        <div className="container mt-5">
            <h2>All Products</h2>
            <button className="btn btn-success mb-3" onClick={createProduct}>Create Product</button>
            <table className="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {products.map(product => (
                            <tr key={product._id}>
                                <td>{product.name}</td>
                                <td>{product.description}</td>
                                <td>${product.price ? product.price.toFixed(2) : 'N/A'}</td>
                                <td>{product.isActive ? "Active" : "Inactive"}</td>
                                <td>
                                    {product.isActive ? (
                                        <button className="btn btn-warning mr-2" onClick={() => archiveProduct(product._id)}>Deactivate</button>
                                    ) : (
                                        <button className="btn btn-success mr-2" onClick={() => activateProduct(product._id)}>Activate</button>
                                    )}
                                    <button className="btn btn-primary" onClick={() => updateProduct(product)}>Update</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
    }

    export default AdminControlPanel;
