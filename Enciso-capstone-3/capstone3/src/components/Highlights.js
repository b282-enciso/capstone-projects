import{Row, Col, Card} from 'react-bootstrap';

export default function Highlights() {
    return (
        <Row className="mt-3 mb-3">
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Frontend Books</h2>
                        </Card.Title>
                        <Card.Text>
                          Immerse yourself in the world of Frontend Development with our curated books. From HTML, CSS, and JavaScript essentials to design principles, these guides lead to captivating user experiences and mastering frontend art.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Backend Books</h2>
                        </Card.Title>
                        <Card.Text>
                            Discover backend development's realm through our selected books. Dive into databases, server logic, API creation, and more. Whether beginner or pro, these texts empower building, maintaining functional web backbones.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}>
                <Card className="cardHighlight p-3">
                    <Card.Body>
                        <Card.Title>
                            <h2>Fullstack Books</h2>
                        </Card.Title>
                        <Card.Text>
                            Craft engaging frontend experiences with HTML, CSS, JavaScript. Construct backend structures, handle databases, APIs. These books light the fullstack path, creating web solutions with seamless interfaces and potent server capabilities.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
    )
}

