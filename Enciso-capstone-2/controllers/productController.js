const Product = require("../models/Product");

// Utility function to check if user is admin
const ensureAdmin = data => {
    if (!data.isAdmin) {
        throw new Error("User must be Admin to access this.");
    }
}

// Create a product
module.exports.addProduct = async (data) => {
    ensureAdmin(data);

    const newProduct = new Product({
        name: data.product.name,
        description: data.product.description,
        price: data.product.price
    });

    try {
        await newProduct.save();
        return true;
    } catch (error) {
        return false;
    }
};

module.exports.getAllProducts = async () => {
    try {
        const products = await Product.find({});
        return products;
    } catch (error) {
        throw new Error("Failed to retrieve all products.");
    }
};

module.exports.getAllActive = async () => {
    try {
        const products = await Product.find({isActive: true});
        return products;
    } catch (error) {
        throw new Error("Failed to retrieve active products.");
    }
};

module.exports.getProduct = async (reqParams) => {
    try {
        const product = await Product.findById(reqParams.productId);
        return product;
    } catch (error) {
        throw new Error("Failed to retrieve the product.");
    }
};

module.exports.updateProduct = async (productId, data) => {
    ensureAdmin(data);

    const updatedProduct = {
        name: data.product.name,
        description: data.product.description,
        price: data.product.price
    };

    try {
        await Product.findByIdAndUpdate(productId, updatedProduct);
        return true;
    } catch (error) {
        return false;
    }
};

module.exports.archiveProduct = async (productId, data) => {
    ensureAdmin(data);

    try {
        await Product.findByIdAndUpdate(productId, {isActive: false});
        return true;
    } catch (error) {
        return false;
    }
};

module.exports.activateProduct = async (productId, data) => {
    ensureAdmin(data);

    try {
        await Product.findByIdAndUpdate(productId, {isActive: true});
        return true;
    } catch (error) {
        return false;
    }
};
