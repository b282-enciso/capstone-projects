const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.checkEmailExists = async (reqBody) => {
    try {
        const users = await User.find({email: reqBody.email});
        return users.length > 0;
    } catch (error) {
        throw new Error("Failed to check if email exists.");
    }
};

module.exports.registerUser = async (reqBody) => {
    const newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    });

    try {
        await newUser.save();
        return true;
    } catch (error) {
        return false;
    }
};

module.exports.loginUser = async (reqBody) => {
    try {
        const user = await User.findOne({email: reqBody.email});
        if (!user) {
            return false;
        }

        const isPasswordCorrect = bcrypt.compareSync(reqBody.password, user.password);
        if (isPasswordCorrect) {
            return { access: auth.createAccessToken(user) };
        } else {
            return false;
        }
    } catch (error) {
        throw new Error("User authentication failed.");
    }
};


module.exports.checkout = async (data, userId) => {
    const user = await User.findById(userId);
    if (user.isAdmin) {
        return false;
    }

    try {
        user.orderedProduct.push({
            products: [{
                productId: data.productId,
                productName: data.productName,
                quantity: data.quantity,
            }],
            totalAmount: data.totalAmount,
        });

        await user.save();

        const product = await Product.findById(data.productId);
        product.userOrders.push({userId: userId});
        await product.save();

        return true;
    } catch (error) {
        return false;
    }
};

module.exports.getProfile = async (data) => {
    try {
        const user = await User.findById(data.userId);
        if (!user) {
            throw new Error("User not found.");
        }

        user.password = undefined;
        return user;
    } catch (error) {
        throw new Error("Failed to retrieve user profile.");
    }
};
