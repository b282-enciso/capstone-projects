const jwt = require("jsonwebtoken");
const axios = require('axios');

const secret = "CapstoneProject";

module.exports.createAccessToken = (user) => {
  const data = {
    id : user._id,
    email : user.email,
    isAdmin : user.isAdmin
  };

  return jwt.sign(data, secret, {});
};


// Token Verification
module.exports.verify = (req, res, next) => {

  let token = req.headers.authorization;

  if(typeof token !== "undefined") {
    console.log(token); 
    token = token.slice(7, token.length);
    
    // Validate the token using the "verify" method decrypting the token using the secret code
    return jwt.verify(token, secret, (err, data) => {
      // If JWT is not valid
      if(err) {
        return res.status(401).send({message: "Unauthorized: Token verification failed"});
      // If JWT is valid
      } else {
        
        next();
      }
    })
  // Token does not exist
  } else {
    return res.status(401).send({message: "Unauthorized: Token verification failed"});
  };
};


// Token decryption
module.exports.decode = (token) => {
  if (typeof token !== "undefined") {

    token = token.slice(7, token.length);
    return jwt.verify(token, secret, (err, data) => {
      if (err) {
        return null;
      } else {

        return jwt.decode(token, {complete:true}).payload;
      }
    })

  } else {
    return null;
  };
};


// Added login function
module.exports.login = async (email, password) => {
    try {
        const response = await axios.post('/api/login', { email, password });
        return response.data;
    } catch (error) {
        throw new Error(error.response.data.message || "Login failed");
    }
};



