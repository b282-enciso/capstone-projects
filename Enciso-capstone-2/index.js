const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");


// Allows access to routes defined within our application
const userRoute = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");

const app = express();

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://innahenciso:EO3GzfTbn4saD0MU@wdc028-course-booking.lgil3yq.mongodb.net/Capstone2",

  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Allows all resources to access our backend application
app.use(cors());

app.use("/users", userRoute);
app.use("/products", productRoute);


// Catch-all for unknown routes
app.all('*', (req, res) => {
    res.status(404).send('Endpoint not found');
});

// "process.env.PORT" is an environment variable that typically holds the port number on which the server should listen.
app.listen(process.env.PORT || 4001, () => console.log(`Now listening to port ${process.env.PORT || 4001}!`));
